package org.paumard.elevator.model;

import java.util.Objects;
import java.util.function.*;

public enum DIRECTION {

    UP(
            (currentFloor, nextFloor) -> {
                if (currentFloor > nextFloor) {
                    System.out.printf("Going UP from %d to %d, exiting...\n", currentFloor, nextFloor);
                    System.exit(0);
                }
            }
    ),
    STOP(
            (currentFloor, nextFloor) -> {
                if (!Objects.equals(currentFloor, nextFloor)) {
                    System.out.printf("Stopping from %d to %d, exiting...\n", currentFloor, nextFloor);
                    System.exit(0);
                }
            }
    ),
    DOWN(
            (currentFloor, nextFloor) -> {
                if (currentFloor < nextFloor) {
                    System.out.printf("Going DOWN from %d to %d, exiting...\n", currentFloor, nextFloor);
                    System.exit(0);
                }
            }
    );

    private BiConsumer<Integer, Integer> validateNextFloor;

    DIRECTION(BiConsumer<Integer, Integer> validateNextFloor) {
        this.validateNextFloor = validateNextFloor;
    }

    public void validateNextFloor(int currentFloor, int nextFloor) {
        this.validateNextFloor.accept(currentFloor, nextFloor);
    }
}
