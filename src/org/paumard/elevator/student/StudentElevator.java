package org.paumard.elevator.student;

import org.paumard.elevator.Elevator;
import org.paumard.elevator.model.Person;

import java.time.LocalTime;
import java.util.List;

public class StudentElevator implements Elevator {

    private final int elevatorCapacity;
    private final String id;
    private Elevator otherElevator;

    public StudentElevator(int elevatorCapacity, String id) {
        this.elevatorCapacity = elevatorCapacity;
        this.id = id;
    }

    public StudentElevator(int elevatorCapacity, String id, Elevator otherElevator) {
        this.elevatorCapacity = elevatorCapacity;
        this.id = id;
        this.otherElevator = otherElevator;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void startsAtFloor(LocalTime time, int initialFloor) {
    }

    @Override
    public void peopleWaiting(List<List<Person>> peopleByFloor) {
    }

    @Override
    public List<Integer> chooseNextFloors() {
        return List.of(1);
    }

    @Override
    public void arriveAtFloor(int floor) {
    }

    @Override
    public void loadPeople(List<Person> person) {
    }

    @Override
    public void unload(List<Person> person) {
    }

    @Override
    public void newPersonWaitingAtFloor(int floor, Person person) {
    }

    @Override
    public void lastPersonArrived() {
    }

    @Override
    public void timeIs(LocalTime time) {
    }

    @Override
    public void standByAtFloor(int currentFloor) {
    }
}
