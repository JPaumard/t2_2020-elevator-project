## Le problème de l'ascenseur

### Le projet

Les habitants de la paisible Résidence des Peupliers vivent une situation assez étrange. Des dysfonctionnements répétés de leur ascenseur leur causent des désagréments dans leur vie quotidienne. Ils font appel à une société de maintenance afin d'y remédier. La société de maintenance a un peu changé leur vie, puisque pour faire des expérimentations ils ont ajouté quelques étages de bureaux à leur paisible résidence, qui en compte à présent 10 (étages, pas habitants). 

### Fonctionnement de l'ascenseur

L'action se déroule dans la Poplar Tech Residence, un immeuble de bureaux de 10 étages. 

#### Plusieurs ascenseurs

Afin de palier les problèmes d'attentes trop longues, l'immeuble a décidé de se doter de 2 ascenseurs, qui peuvent avoir chacun leur propre algorithme de fonctionnement. 

Le fonctionnement de chaque ascenseur pris individuellement est inchangé par rapport à l'étape 4. 

Le système commence par instancier un premier ascenseur (classe `Building` ligne 35-37). Puis il en instancie un second, en passant le premier ascenseur en paramètre de la construction du second. Cela permet aux deux ascenseurs de se connaître et de se synchroniser pour se partager le transport des résidents. 


#### Mouvements de l'ascenseur

L'ascenseur peut s'arrêter à chaque étage. 

L'ascenseur peut emporter 15 personnes. Au-delà la sécurité n'est plus assurée. 

Une personne peut attendre l'ascenseur à chaque étage. Elle peut appeler l'ascenseur en appuyant un bouton qui indique l'étage où elle veut se rendre.  

Il se passe les choses suivantes lorsque l'ascenseur arrive à un étage. 

1. Les personnes qui souhaitent se rendre à cette étage descendent de l'ascenseur. 
2. L'ascenseur indique les 5 prochains étages qu'il va visiter. 
3. Les personnes qui attendent à cet étage et souhaitent se rendre à l'un des étages desservis par l'ascenseur peuvent monter, s'il reste de la place. 
4. Lorsque tout le monde est chargé l'ascenseur ferme ses portes. 
5. L'ascenseur se rend alors au premier étage qu'il a choisi. 
6. Si l'ascenseur annonce qu'il se rend à l'étage où il se trouve déjà, alors il se met en attente.   

#### Durées de fonctionnement

##### Mouvements de l'ascenseur

L'ascenseur met un certain temps à traverser les étages. 

* Un ascenseur à l'arrêt qui monte ou descend à l'étage suivant met 18s.
* Un ascenseur à l'arrêt met 12s changer étage.
* Un ascenseur en mouvement met 6s pour changer d'étage.
* Un ascenseur en mouvement accélère et met 3s pour changer d'étage. 
* Un ascenseur en mouvement met 12s pour changer d'étage et s'y arrêter. 

Quelques exemples de calcul. 
 
| Etage de départ | Etage d'arrivée | Temps de trajet |
| --------------- | --------------- | --------------- |
| 1 | 2 | 18s |
| 1 | 3 | 30s = 12 + 6 + 12 |
| 1 | 4 | 36s = 12 + 6 + 6 + 12 |
| 1 | 5 | 39s = 12 + 6 + 3 + 6 + 12 |
| 4 | 3 | 18s |
| 3 | 1 | 30s = 12 + 6 + 12 |
| 4 | 1 | 36s = 12 + 6 + 6 + 12 |
| 5 | 1 | 39s = 12 + 6 + 3 + 6 + 12 |

##### Chargement et déchargement

Le temps de chargement et de déchargement des personnes prend également du temps. 

Les personnes peuvent monter et descendre de l'ascenseur 3 par 3. 

* Ouvrir ou fermer les portes prend 3 secondes. 
* Le chargement des trois premières personnes prend 9 secondes. 
* Le chargement de trois personnes supplémentaires prend 6 secondes.   
* Le déchargement des trois permières personnes prend 9 secondes. 
* Le déchargement de trois personnes supplémentaires prend 6 secondes.

On doit attendre que toutes les personnes soient sorties de l'ascenseur avant de pouvoir faire monter d'autres personnes. 

Quelques exemples de calcul.   

| Personne à charger | Personne à décharger | Durée |
| --------------- | --------------- | --------------- |
| 0 | 0 | 6s = 3 + 3 |
| 1 | 0 | 15s = 3 + 9 + 3 |
| 2 | 0 | 15s = 3 + 9 + 3 |
| 3 | 0 | 15s = 3 + 9 + 3 |
| 5 | 0 | 21s = 3 + 9 + 6 + 3 |
| 0 | 1 | 15s = 3 + 9 + 3 |
| 0 | 2 | 15s = 3 + 9 + 3 |
| 0 | 3 | 15s = 3 + 9 + 3 |
| 0 | 5 | 21s = 3 + 9 + 6 + 3 |
| 7 | 5 | 42s = 3 + 9 + 6 + 6 + 9 + 6 + 3 |


### Fonctionnement du programme

#### Scénario de fonctionnement

L'ascenseur fonctionne durant toute une journée et dessert un immeuble de bureaux. 

Au démarrage les files d'attente sont vides, et les personnes commencent à arriver. L'ascenseur démarre de l'étage 1. 

La journée commence à 6h00 et se termine à 22h30. A 23h30 le système s'arrête, qu'il reste des personnes à attendre ou non. 

Lors d'une journée de travail, les personnes arrivent le matin à l'étage 1 et se rendent à leur bureau. Elles repartent à partir de 16h30, en appelant l'ascenseur de leur bureau pour l'étage 1. 

Le système prend en compte cette réalité. L'affluence relative est lue dans un fichier `files/affluence.txt` qui donne l'affluence en fonction de l'heure. À 6h l'affluence relative est de 0.0167. À 7h elle est de 0.0667, soit 6 fois plus élevée. À 8h elle est de 0.6000 soit 10 fois plus élevée qu'à 7h. 

Si l'on regarde cette affluence on se rend compte qu'il y a un pic important le matin, un pic plus petit à l'heure du déjeuner, et que le pic de la fin de journée est un peu plus dilué que celui du matin. 

Jusqu'à 10h30 le matin, 80% des personnes qui prennent l'ascenseur le prenne à l'étage 1 pour monter à leur bureau. Les 20% restants prennent l'ascenseur normalement. 

À partir de 16h30, 90% des personnes prennent l'ascenseur pour se rendre à l'étage 1. Les 10% restant prennent l'ascenseur normalement. 

Ces données d'affluence sont des données réelles fournies par la RATP pour l'affluence dans le quartier d'affaires de la Défense. 

#### La classe `Building`

Le fonctionnement de l'ascenseur est programmé dans une classe `Building` qui comporte une méthode `main()`.  

Dans un premier temps, la classe `Building` crée un immeuble de 10 étages avec un ascenseur et crée des files d'attente de personnes à chaque étage. Une file d'attente peut être vide. Les files d'attente changent au cours du temps car de nouvelles personnes peuvent se présenter. 

#### L'interface `Elevator`

La classe `Building` fonctionne avec une instance de l'interface `Elevator`. Cette interface comporte plusieurs méthodes que la classe `Building` appelle dans l'ordre suivant. La classe `Building` connaît le nombre maximal de personnes que l'ascenseur peut embarquer.

##### Etape 1 : initialisation

Les étages sont numérotés de 1 à 10.

En début de programme la méthode `startsAtFloor()` de l'interface `Elevator` est appelée avec le numéro de l'étage duquel l'ascenseur part.  

La méthode `peopleWaiting()` de l'interface `Elevator` est ensuite appelée avec la liste des files d'attente à chaque étape. Pour un immeuble de 10 étages, cette liste comporte donc 10 éléments, un par étage. Chacun de ces éléments est lui-même une liste des personnes qui attendent à cet étage.  

##### Etape 2 : choix de l'étage de destination

Ensuite, la méthode `chooseNextFloor()` est appelée. Elle retourne la liste des étages où l'ascenseur se rend. Chaque étage est numéroté de 1 à 10. 
 
Cette liste peut comporter de 1 à 5 étages. Si un ascenseur retourne plus de 5 étages, seuls les 5 premiers seront pris en compte. Si celle liste est vide alors l'étage 1 y est ajouté. Dans les deux cas un message d'erreur signale le problème. 

Si l'appel à cette méthode retourne l'étage où l'ascenseur se trouve déjà, alors l'ascenseur se met en attente.  

##### Etape 3 : chargement des personnes
 
La classe `Building` regarde ensuite les personnes qui attendent à l'étage où se trouve l'ascenseur. Elle détermine les personnes qui vont monter dans l'ascenseur, en fonction des destinations de l'ascenseur et de la place disponible qu'il lui reste. Elle appelle la méthode `loadPerson()` de l'interface `Elevator` avec une liste de  personnes en paramètre. 

Si 3 personnes montent dans l'ascenseur, la méthode `loadPerson()` sera appelée une fois avec les trois personnes dans une liste. Si 5 personnes montent dans l'ascenseur alors la méthode `loadPerson()` sera appelée deux fois avec une liste de 3 personnes, puis une liste de 2 personnes. 

Si deux ascenseurs se trouvent au même étage au même moment, et annoncent qu'ils desservent des étages en commun, alors certaines personnes vont avoir à choisir entre prendre un ascenseur ou un autre. 

Ce choix se fait de la façon suivante : 

- le premier critère est qu'il y a de la place dans l'ascenseur : si l'un des deux est plein, c'est bien sûr l'autre qui sera choisi ;
- le deuxième critère est la rapidité : si les deux ascenseurs ont de la place, la personne choisira l'ascenseur qui l'emmènera le plus vite à destination ;
 - le troisième critère est l'occupation : si les deux ascenseurs peuvent emmener cette personne à la même vitesse, alors elle prendra l'ascenseur le moins plein ;
 - enfin le quatrième critère est un tirage au sort : si les deux ascenseurs ont le même taux de remplissage, alors la personne en choisira un au hasard. 

##### Etape 4 : déplacement de l'ascenseur, arrivée à un nouvel étage

La classe `Building` déplace ensuite l'ascenseur vers sa première destination, telle qu'elle a été choisie à l'étape 2. Elle appelle la méthode `arriveAtFloor()` de l'interface `Elevator` avec le numéro de l'étage où l'ascenseur arrive.

##### Etape 5 : déchargement des personnes
 
La classe `Building` fait sortir les personnes de l'ascenseur qui souhaitent aller à cet étage. Elle appelle la méthode `unloadPerson()` de l'interface `Elevator` avec la liste des personnes qui descendent.
 
 Si 3 personnes descendent de l'ascenseur, la méthode `unloadPerson()` est appelée une fois avec la liste des trois personnes qui descendent. Si 5 personnes descendent de l'ascenseur, alors la méthode `unloadPerson()` est appelée 2 fois : une première fois avec une liste de trois persones, et une seconde avec une liste de deux personnes. 

##### Etape 6 : arrivée de nouvelles personnes

Il se peut que de nouvelles personnes se présentent aux étages pour prendre l'ascenseur. Dans ce cas, la méthode `newPersonWaitingAtFloor()` de l'interface `Elevator` est appelée, avec en paramètre l'étage où cette personne se présente et la personne qui attend.  

Le programme reprend alors à l'étape 2. 

##### Etape 7 : mise en attente de l'ascenseur 

Si l'ascenseur a retourné l'étage auquel il se trouve déjà, c'est qu'il souhaite se mettre en attente. Cela peut arriver quand l'ascenseur a fini de transporter toutes les personnes qui étaient en attente, mais que d'autres personnes sont susceptibles d'arriver.

Quand une personne appelle l'ascenseur alors qu'il est en attente, alors la méthode `peopleWaiting()` est appelée normalement et le processus reprend à l'étape 2. 


##### Etape 8 : fin de l'arrivée des personnes

Lorsque le système a fini de générer des personnes, la méthode `lastPersonArrived()` est appelée ce qui informe l'ascenseur.

L'ascenseur peut continuer à fonctionner normalement. Il lui alors reste 1 heure pour terminer de transporter les personnes encore présentes dans les files d'attente et rejoindre l'étage 1 où il doit se mettre en attente. 
 
##### Appel de l'ascenseur.

À chaque instant une personne peut se présenter à un étage et appeler l'ascenseur. Cette arrivée est aléatoire. Dans ce cas l'ascenseur en est informé : la méthode `peopleWaiting()` est appelée avec l'étage où se trouve cette personne, et la personne elle-même. 

##### Arrêt de l'ascenseur.

Si l'ascenseur se trouve en attente lorsque que la méthode `lastPersonArrived()` a été appelée, ou qu'il se met en attente alors que la méthode `lastPersonArrived()` a déjà été appelée, alors il s'arrête. 

La classe `Building` affiche alors le contenu des files d'attente des étages et le contenu de l'ascenseur, de même que l'heure qu'il est.  

### Eléments fournis

Le projet fourni comporte les classes suivantes, qui ne doivent pas être modifiées.

- Classe `Building`, `ShadowElevator`, `WaitingList` et `Event` : ces classes implémentent les étapes décrites.
- Classe `Person` : cette classe modélise une personne qui attend l'ascenseur. Chaque personne possède un nom et l'étage auquel elle veut se rendre. 
- Enumération `DIRECTION` : cette énumération modélise les mouvements possibles de l'ascenseur. 
- Interface `Elevator` : cette interface fixe les méthodes que doit implémenter votre classe. Elle ne doit pas être modifiée. 
- Classe `DumbElevator` : un exemple d'implémentation de l'interface `Elevator` qui fonctionne de temps en temps... 

Si l'on appelle :

```java
WaitingList peopleWaitingPerFloor = new WaitingList();
```  

Alors des files d'attentes aléatoires sont générées pour chaque étage, qui peuvent être vides.

### Implémenter `Elevator`

Le projet consiste à implémenter l'interface `Elevator` et à fournir cette implémentation. 

### Travail à fournir

Votre travail consiste à améliorer ce fonctionnement, de façon à garantir que toute personne qui attend l'ascenseur soit amenée à l'étage qu'elle souhaite. Lorsque le système s'arrête, l'ascenseur doit retourner à l'étage 1 et s'arrêter. Il est possible de mettre l'ascenseur en attente à un autre étage que le 1, tant qu'à la fin de la simulation il se rende à l'étage 1. 

Dans la pratique, vous devez fournir une implémentation de l'interface `Elevator` qui implémente ce comportement. 

Vous avez la possibilité de changer les lignes 35-37 de la classe `Building` afin de créer les ascenseurs comme vous l'entendez. Les contraintes sont les suivantes : 

- vous ne devez pas créer plus de deux ascenseurs ;
- vous pouvez créer deux instances de votre classe qui implémente `Elevator`, mais vous ne pouvez pas passer deux fois la même instance. 

Donc les lignes 35 à 37 peuvent être les suivantes :

```java
Elevator elevator1 = new StudentElevator(ELEVATOR_CAPACITY, "STUDENT 1 1");
Elevator elevator2 = new StudentElevator(ELEVATOR_CAPACITY, "STUDENT 1 2");
Elevators elevators = new Elevators(List.of(elevator1, elevator2));
```

Ou encore :

```java
Elevator elevator1 = new StudentElevatorV1(ELEVATOR_CAPACITY, "STUDENT V1");
Elevator elevator2 = new StudentElevatorV2(ELEVATOR_CAPACITY, "STUDENT V2", elevator1);
Elevators elevators = new Elevators(List.of(elevator1, elevator2));
```

Mais elles ne peuvent pas être :

```java
Elevator elevator = new StudentElevator(ELEVATOR_CAPACITY, "STUDENT 1 1");
Elevators elevators = new Elevators(List.of(elevator, elevator));
```

L'interface `Elevator` possède une méthode `getId()` qui retourne une chaîne de caractères. Cette méthode est utilisée dans les informations de sortie, pour indiquer notamment quel ascenseur fait quoi. 

Dans les informations suivantes : 

```
[19:04:51] Elevator [Student V1] door closed at floor 3, going to floor 2
[19:04:54] Elevator [Student V2] door opened at floor 2
[19:04:54] Elevator [Student V2] decides to go to floor [3]
[19:04:54] Elevator [Student V2] going UP to floor 3 from floor 2
[19:04:57] Elevator [Student V2] door closed at floor 2, going to floor 3
[19:05:09] Elevator [Student V1] arrived at floor 2
```

On voit que certaines informations sont relatives à l'ascenseur dont l'ID est `Student V1` et d'autres relatives à l'ascenseur dont l'ID est `Student V2`.

Même si le système ne vérifie pas ce point, il est donc important de s'assurer que chaque ascenseur possède un ID différent. 

### Remarques

Pour cette cinquième étape, une liste d'attente des personnes est fixée au démarrage du système et change au fil du temps. Votre ascenseur donne les étages auxquels il se rend quand il arrive à un étage. Les personnes montent dans l'ascenseur en fonction des étages qu'il donne. 

L'arrivée des personnes dans les files d'attente est aléatoire, ce qui rend les tests difficiles. Vous pouvez fixer la variable aléatoire (classe `WaitingList` ligne 20) de façon à ce que les files d'attente générées soient toujours les mêmes d'une exécution à l'autre de votre programme.

Le code suivant : 
```java
private static Random random = new Random();
```
crée une série de nombres aléatoires qui varie avec chaque exécution. 

En revanche, le code suivant : 
```java
private static Random random = new Random(10L);
```
génèrera toujours la même suite de nombres aléatoires. On peut changer la valeur `10L` par une autre, ce qui génèrera une autre suite de nombres aléatoires, différente de la première. 

### Evaluation

L'évaluation se fera sur des files d'attente aléatoires, fixées au démarrage de l'application et qui seront les mêmes pour tout le monde. L'arrivée aléatoire des personnes aux étages sera également la même pour tout le monde. 

Les critères seront les suivants :

- l'ascenseur s'arrête au niveau 1 ;
- il est vide ;
- plus personne n'attend dans les étages ;
- le temps d'attente moyen est minimal ;
- le temps d'attente maximum est minimal.  

Le système vous fourni des informations sur les performances de votre ascenseur, sous la forme suivante : 
````
[22:30] Stopping at floor 1
People waiting on floor 1
	No one
People waiting on floor 2
	No one
People waiting on floor 3
	No one
People waiting on floor 4
	No one
People waiting on floor 5
	No one
People waiting on floor 6
	No one
People waiting on floor 7
	No one
People waiting on floor 8
	No one
People waiting on floor 9
	No one
People waiting on floor 10
	No one
Elevator stopped at floor 1 No one left in the elevator
[22:30:06] Times up
People loaded: 420
Max people loaded: 8
 0h  0mn 30s -> 169
 0h  3mn  0s -> 130
 0h  6mn  0s -> 79
 0h 15mn  0s -> 20
 0h 30mn  0s -> 2
 1h  0mn  0s -> 0
Number of people taken = 420
Average waiting time = 12mn 55s
Max waiting time = 0h 53mn 30s
````

1. `Max people loaded` est le nombre maximal que l'ascenseur à chargé à la fois. 
2. `Number of people taken` est le nombre total de personnes transportées. 
3. `Average waiting time` est le temps moyen d'attente pour une personne. 
4. `Max waiting time` est le temps d'attente maximal. 53mn à attendre un ascenseur... Les gens sont patients !

### Stratégie

Le système est réglé pour générer environ 400 à 500 personnes sur la tranche horaire 6h-22h30.  

Bonne nouvelle : les utilisateurs sont des gens patients, prêts à attendre leur ascenseur des heures plutôt que de prendre l'escalier. Cela dit, programmer un ascenseur qui se borne à visiter les étages sans prendre en compte l'affluence et les files d'attente va générer un fort mécontentement de ces utilisateurs. De plus, il est possible qu'il ne soit pas capable de transporter les 400 (environ) personnes qui vont se présenter.

Vous avez tous les éléments en main pour programmer un système d'ascenseurs un peu plus intelligent : à vous de jouer !
